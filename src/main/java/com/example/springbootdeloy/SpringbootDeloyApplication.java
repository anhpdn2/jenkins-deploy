package com.example.springbootdeloy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringbootDeloyApplication {

    @GetMapping("/")
    public String welcome() {
        return "Welcome to the summoner rift";
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDeloyApplication.class, args);
    }

}
